import API from '../../api';
import { useState, useEffect } from 'react';
import Headers from '../utils/Headers';
import './Users.css';

const Users = () => {
  const [ users, setUsers ] = useState([]);

  useEffect(() => {
    const getUsers = async () => {
      const res = await API.get('user?page=1&limit=20', {
        headers: {
          'app-id': '6138db75310f13e622e93f63'
        }
      })
      const data = res.data.data;
      setUsers(data);
    }
    getUsers();
  }, [])

  return (
    <>
      <Headers title="USERS" />
      <div className="row flex-wrap">
        {
          users.map(user => (
            <div key={user.id} className="content-flex-card padding-10 row">
              <div>
                <img src={user.picture} alt={user.firstName} className="img-rounded" />
              </div>
              <div className="customize-user">
                <p><span className="text-transform-capitalize">{user.title}</span> {user.firstName} {user.lastName}</p>
              </div>
            </div>
          ))
        }
      </div>
      
    </>
  )
}

export default Users;