import React, { useState } from 'react';
import './Modal.css';

const Modal = ( props ) => {
  const { text } = props;
  const [show, setShow] = useState(false);

  const handleModalClose = (e) => {
    setShow(false);
  };
  
  const handleModalOpen = () => {
    setShow(true);
  };

  return (
    <div className="modal">
      <div hidden={!show} >
        <div
          className="modal-background"
          onClick={handleModalClose}
        >
          <div className="modal-card">
            {props.children}
          </div>
        </div>
      </div>
      <p
        className="pointer"
        style={{"fontSize": "13px", "color": "blue"}}
        onClick={handleModalOpen}  
      >
        {!text ? 'See More' : text}
      </p>
    </div>
  )
}

export default Modal;