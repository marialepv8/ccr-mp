const Headers = ( props ) => {
  const { title } = props;
  return (
    <>
      <h2 className="text-align-center">{title}</h2>
      <br />
      <div className="line" />
      <br /><br /><br />
    </>
  )
}

export default Headers;