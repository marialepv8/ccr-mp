import { GoogleLogin } from 'react-google-login';
import { useHistory } from 'react-router-dom';
import './Login.css';

const Login = ( props ) => {
  /* const { setToken, setUser } = props; */
  const history = useHistory();

  const responseGoogle = (response) => {
    console.log(response);
    console.log(response.profileObj);
    history.push('/home');
  }

  return (
    <div className="login">
      <h1>Welcome!</h1>
      <br /><br />
      <p>We invite you to <b>"Sign in with Google"</b></p>
      <br /><br /><br />
      <GoogleLogin
        clientId="622698158855-bii5p7ol2k4u6pneaomb8q9sr12ga3sf.apps.googleusercontent.com"
        buttonText="Sign in"
        onSuccess={responseGoogle}
        onFailure={responseGoogle}
        cookiePolicy={'single_host_origin'}
      />  
    </div>
  )
}

export default Login;