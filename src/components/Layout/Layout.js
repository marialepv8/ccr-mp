import Header from "./Header";
import './Layout.css';

const Layout = ( props ) => {
  return (
    <>
      <Header />
      <div className="content">
        <div className="content-card">
          {props.children}
        </div>
      </div>
    </>
  )
}

export default Layout;