import { Link } from 'react-router-dom';
import './Header.css';

const Header = () => {
  return (
    <div className="header color-white">
      <div className="row">
        <Link to="/home" className="header-link color-white">Home</Link>
        <Link to="/users" className="header-link color-white">Users</Link>
        <Link to="/comments" className="header-link color-white">Comments</Link>
      </div>
      <div>Usuario y Foto</div>
    </div>
  )
}

export default Header;