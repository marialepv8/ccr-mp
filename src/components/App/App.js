import { Route, Switch } from 'react-router-dom';
/* import { useHistory } from 'react-router'; */
import Login from '../Login/Login';
import Home from '../Home/Home';
import Users from '../User/Users';
import Comments from '../Comment/Comments';
/* import useToken from './useToken'; */
import './App.css';
import Layout from '../Layout/Layout';

const App = () => {
  /* const { token, setToken } = useToken();
  const history = useHistory();

  if(!token) {
    history.push('/');
    localStorage.clear();
  } */

  return (
    <Layout>
      <Switch>
        <Route exact path="/">
          <Login component={Login} />
        </Route>
        <Route path="/home">
          <Home component={Home} />
        </Route>
        <Route path="/users">
          <Users component={Users} />
        </Route>
        <Route path="/comments">
          <Comments component={Comments} />
        </Route>
      </Switch>
    </Layout>
  );
}

export default App;
