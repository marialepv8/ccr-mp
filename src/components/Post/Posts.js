import { useState, useEffect } from 'react';
import API from '../../api';
import Headers from '../utils/Headers';
import Modal from '../utils/Modal';
import Post from './Post';
import './Posts.css';

const Posts = ( ) => {
  const [ posts, setPosts ] = useState([]);

  useEffect(() => {
    const getPosts = async () => {
      const res = await API.get('post?page=1&limit=20', {
        headers: {
          'app-id': '6138db75310f13e622e93f63'
        }
      })
      const data = res.data.data;
      console.log(data)
      setPosts(data);
    }
    getPosts();
  }, [])

  

  return (
    <>
      <Headers title="POSTS" />
      <div className="row flex-wrap">
        {
          posts.map(post => (
            <div key={post.id} className="content-flex-card">
              <div className="col">
                <div className="row padding-10">
                  <div>
                    <img src={post.owner.picture} alt={post.owner.firstName} className="img-rounded" />
                  </div>
                  <div className="col customize-post-user">
                    <div>
                      <p><span className="text-transform-capitalize">{post.owner.title}</span> {post.owner.firstName} {post.owner.lastName}</p>
                    </div>
                    <div>{new Date(post.publishDate).toLocaleString()}</div>
                  </div>
                </div>
                <div>
                  <img src={post.image} alt={post.image} className="post-image" />
                </div>
                <div className="col padding-10"> 
                  <div className="row flex-wrap" style={{"fontSize": "14px"}}>
                    <div style={{"marginRight": "5px"}}>{post.likes}</div>
                    <div className="row">
                      {
                        post.tags.map((tag, index) => (
                          <div key={index} style={{"margin": "0 5px", "backgroundColor": "lightgray", "borderRadius": "5px", "padding": "1px"}}>
                            {tag}
                          </div>
                        ))
                      }
                    </div>
                  </div>
                  <div>
                    <p className="text-align-justify" style={{"fontSize": "12px", "color": "gray"}}>{post.text}</p>
                    <Modal><Post id={post.id}/></Modal>
                  </div>
                </div>
              </div>
            </div>
          ))
        }
      </div>
      
    </>
  )
}

export default Posts;