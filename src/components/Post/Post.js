import API from '../../api';
import { useState, useEffect } from 'react';
import './Posts.css';

const Post = ( props ) => {
  const { id } = props;
  const [ post, setPost ] = useState({});
  const [ postOwner, setPostOwner ] = useState({});
  const [ postTags, setPostTags ] = useState([]);
  const [ comments, setComments ] = useState([]);
  

  useEffect(() => {
    const getPost = async () => {
      const res = await API.get(`post/${id}`, {
        headers: {
          'app-id': '6138db75310f13e622e93f63'
        }
      })

      const data = res.data;
      setPost(data);
      setPostOwner(data.owner);
      setPostTags(data.tags);
      getCommentsByPost();
    }
    getPost();
  }, []) //eslint-disable-line
  
  const getCommentsByPost = async () => {
    const res = await API.get(`post/${id}/comment`, {
      headers: {
        'app-id': '6138db75310f13e622e93f63'
      }
    })

    const data = res.data;
    setComments(data.data);
  }

  return (
    <div>
      <div className="col">
        <div className="row ">
          <div>
            <img src={postOwner.picture} alt={postOwner.firstName} className="img-rounded" />
          </div>
          <div className="col customize-post-user">
            <div>
              <p><span className="text-transform-capitalize">{postOwner.title}</span> {postOwner.firstName} {postOwner.lastName}</p>
            </div>
            <div>{new Date(post.publishDate).toLocaleString()}</div>
          </div>
        </div>
        <div>
          <img src={post.image} alt={post.image} 
          style={{"height": "40vh", "width": "100%"}} />
        </div>
        <div className="col"> 
          <div className="row flex-wrap" style={{"fontSize": "14px"}}>
            <div style={{"marginRight": "5px"}}>{post.likes}</div>
            <div className="row">
              {
                postTags.map((tag, index) => (
                  <div key={index} style={{"margin": "0 5px", "backgroundColor": "lightgray", "borderRadius": "5px", "padding": "1px"}}>
                    {tag}
                  </div>
                ))
              }
            </div>
          </div>
          <div>
            <p className="text-align-justify" style={{"fontSize": "12px", "color": "gray"}}>{post.text}</p>
          </div>
          <div>
            {comments.map(comment => (
              <div key={comment.id}>
                {comment.message}
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  )
}

export default Post;