import API from '../../api';
import { useState, useEffect } from 'react';
import Headers from '../utils/Headers';
import './Comments.css';

const Comments = () => {
  const [ comments, setComments ] = useState([]);

  useEffect(() => {
    const getComments = async () => {
      const res = await API.get('comment?page=0&limit=20', {
        headers: {
          'app-id': '6138db75310f13e622e93f63'
        }
      })
      const data = res.data.data;
      setComments(data);
      console.log(data);
    }
    getComments();
  }, [])

  return (
    <>
      <Headers title="COMMENTS" />
      <div className="row flex-wrap">
        {
          comments.map(comment => (
            <div key={comment.id} className="content-flex-card padding-10 col">
              <div className="row">
                <div>
                  <img src={comment.owner.picture} alt={comment.owner.firstName} className="img-rounded" />
                </div>
                <div className="customize-comment">
                  <p>
                    <span className="text-transform-capitalize">{comment.owner.title}</span>
                    {comment.owner.firstName} {comment.owner.lastName}
                  </p>
                  <p>{new Date(comment.publishDate).toLocaleString()}</p>
                </div>
              </div>
              <div>
                {comment.message}
              </div>
            </div>
          ))
        }
      </div>
      
    </>
  )
}

export default Comments;